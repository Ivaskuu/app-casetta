import 'package:flutter/material.dart';
import 'dart:async';

List<String> imagesList = [
  'res/img1.jpg', 'res/img2.png', 'res/img3.png', 'res/img4.png', 'res/img5.png', 
  'res/img6.png', 'res/img7.png', 'res/img8.png', 'res/img9.png', 'res/img10.png', 
];

class ImagePage extends StatefulWidget {
  int heroTag;
  ImagePage(this.heroTag);

  @override
  _ImagePageState createState() => new _ImagePageState();
}

class _ImagePageState extends State<ImagePage> {
  PageController pageController;
  bool showOptions = false;
  bool willPop = false;

  @override
  void initState()
  {
    pageController = new PageController(initialPage: widget.heroTag);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: () {
        setState(() => willPop = true);
        return new Future.value(true);
      },
      child: new Scaffold(
        backgroundColor: willPop ? Colors.transparent : showOptions ? Colors.white : Colors.black,
        body: new GestureDetector(
          onTap: () => setState(() => showOptions = !showOptions),
          child: new Stack(
            children: [
              new PageView.builder(
                controller: pageController,
                onPageChanged: (int pageNum) => setState(() => widget.heroTag = pageNum),
                itemCount: imagesList.length,
                itemBuilder: (_, int pos) => new Hero(tag: pos, child: new Image.asset(imagesList[pos])),
              ),
              !willPop && showOptions ? new Align(
                alignment: Alignment.topCenter,
                child: new Flex(
                  direction: Axis.vertical,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    new AppBar(
                      backgroundColor: Colors.black38,
                      elevation: 0.0,
                      leading: new IconButton(
                        onPressed: () => Navigator.of(context).pop(),
                        icon: new Icon(Icons.arrow_back),
                      ),
                    ),
                  ],
                ),
              ) : new Container(),
              !willPop && showOptions ? new Align(
                alignment: Alignment.bottomCenter,
                child: new Material(
                  color: Colors.black54,
                  child: new Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new IconButton(
                        onPressed: () {},
                        icon: new Icon(Icons.delete, color: Colors.white),
                      ),
                      new IconButton(
                        onPressed: () {},
                        icon: new Icon(Icons.info, color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ) : new Container(),
            ]
          ),
        )
      ),
    );
  }
}