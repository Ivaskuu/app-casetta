import 'package:flutter/material.dart';
import 'gallery_page.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => new _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentPage = 0;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget> [
          new Align(alignment: Alignment.center, child: new GalleryPage()),
          new BottomNavigationBar(
            currentIndex: currentPage,
            fixedColor: Colors.pink,
            type: BottomNavigationBarType.fixed,
            items: [
              new BottomNavigationBarItem(
                icon: new Icon(Icons.image),
                title: new Text('Galleria')
              ),
              new BottomNavigationBarItem(
                icon: new Icon(Icons.movie),
                title: new Text('Ytp')
              ),
            ],
            onTap: (int pageNum) => currentPage != pageNum ? setState(() => currentPage = pageNum) : null,
          ),
          new Align(
            alignment: Alignment.bottomCenter,
            child: new Container(
              margin: new EdgeInsets.only(bottom: 8.0),
              child: new FloatingActionButton(
                onPressed: () {},
                backgroundColor: Colors.red,
                child: new Icon(Icons.add),
              ),
            ),
          )
        ],
      ),
    );
  }
}