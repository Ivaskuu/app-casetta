import 'package:flutter/material.dart';
import 'image_page.dart';

List<String> imagesList = [
  'res/img1.jpg', 'res/img2.png', 'res/img3.png', 'res/img4.png', 'res/img5.png', 
  'res/img6.png', 'res/img7.png', 'res/img8.png', 'res/img9.png', 'res/img10.png', 
];

class GalleryPage extends StatefulWidget
{
  @override
  _GalleryPageState createState() => new _GalleryPageState();
}

class _GalleryPageState extends State<GalleryPage>
{
  @override
  Widget build(BuildContext context)
  {
    return new Scaffold(
      appBar: new AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        title: new Text('Galleria immagini', style: new TextStyle(color: Colors.black, fontSize: 24.0, fontWeight: FontWeight.w700)),
        actions: <Widget>[
          new IconButton(
            onPressed: () {},
            icon: new Icon(Icons.more_vert, color: Colors.black),
          )
        ],
      ),
      body: new Container(
        padding: new EdgeInsets.symmetric(horizontal: 6.0),
        margin: new EdgeInsets.only(bottom: 48.0),
        child: new GridView.builder(
          itemCount: imagesList.length,
          itemBuilder: (_, int pos) => img(imagesList[pos], pos),
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
        )
      )
    );
  }

  Widget img(String imgPath, int heroTag)
  {
    return new InkWell(
      onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new ImagePage(heroTag))),
      child: new Container(
        padding: const EdgeInsets.all(6.0),
        child: new Material(
          child: new GridTile(
            child: new Hero(tag: heroTag, child: new Image.asset(imgPath, fit: BoxFit.cover))
          )
        ),
      ),
    );
  }
}